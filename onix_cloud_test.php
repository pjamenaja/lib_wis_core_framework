#!/usr/bin/php

<?php

chdir('./build');

require_once "onix_core_include.php";

$dat  = new CTable('');
$dat->setFieldValue('PARAM', '');

$flag = new CTable('');
$flag->setFieldValue('FLAG_NAME', 'page-size');
$flag->setFieldValue('FLAG_VALUE', '20');

$flags = [];
array_push($flags, $flag);

$dat->addChildArray('FLAGS', $flags);

GcpCloud::SubmitCommand('projects', '', 'list', '', $dat);

exit (0);
?>
